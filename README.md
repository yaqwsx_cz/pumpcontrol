# PumpControl system

Small AVR-based board for garden pump automation.

## Features:

- Supports two pumps
- Mechanical water level sensing with up to 16 switches
- Real-time clock included for scheduled actions
- WiFi configuration & data collection
- Easy usage with 16x2 character display